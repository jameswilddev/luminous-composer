angular.module('luminous-composer').service('history', function(dialog){
    var done = [];
    var undone = [];
    
    var service = {
        act: function(perform, undo) {
            if(undone.length) {
                dialog.show('undoneChangesWillBeErased', {
                    ok: function(){
                        undone = [];
                        perform();
                        done.push({
                            perform: perform,
                            undo: undo
                        });
                    }
                });
                return;
            }
            
            perform();
            done.push({
                perform: perform,
                undo: undo
            });
        },
        canUndo: function() {
            return done.length;
        },
        canRedo: function() {
            return undone.length;
        },
        undo: function() {
            var last = done[done.length - 1];
            done.splice(done.length - 1, 1);
            undone.push(last);
            last.undo();
        },
        redo: function() {
            var last = undone[undone.length - 1];
            undone.splice(undone.length - 1, 1);
            done.push(last);
            last.perform();
        },
        clear: function() {
            done = [];
            undone = [];
        }
    };
    
    return service;
});