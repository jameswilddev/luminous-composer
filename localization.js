angular.module('luminous-composer').config(function ($translateProvider) {
    $translateProvider.translations('en', {
        undoneChangesWillBeErasedTitle: 'Undone changes',
        undoneChangesWillBeErasedMessage: 'You have undone changes which will be lost if you continue.  Are you sure you want to continue?',
        areYouSureNewTitle: 'Create a new scene?',
        areYouSureNewMessage: 'Are you sure you want to create a new scene?  Any unsaved changes to your current scene will be lost.',
        ok: 'Ok',
        cancel: 'Cancel',
        luminousComposer: 'Luminous Composer',
        browserTitleReplacing: 'Replace with...',
        browserTitleAddingTo: 'Add a new...'
    });
    $translateProvider.preferredLanguage('en');
});