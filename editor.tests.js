describe('editor', function(){
    var scope, rootScope;
    var historyCanUndo, historyCanRedo, historyUndo, historyRedo, historyClear, historyAct;
    var dialog;
    var constructor;
    var linker;
    var functions;
    beforeEach(function(){
        module('luminous-composer');
        inject(function($controller){
            rootScope = {};
            scope = {};
            var history = {
                act: historyAct = jasmine.createSpy(),
                canUndo: historyCanUndo = jasmine.createSpy(),
                canRedo: historyCanRedo = jasmine.createSpy(),
                undo: historyUndo = jasmine.createSpy(),
                redo: historyRedo = jasmine.createSpy(),
                clear: historyClear = jasmine.createSpy()
            };
            dialog = {
            };
            functions = {
                'Test Function A': null,
                'Test Function B': null,
                'Test Function C': null
            };
            linker = jasmine.createSpy();
            linker.and.returnValue('Test Linked Code');
            constructor = jasmine.createSpy();
            constructor.and.returnValue('Test Node');
            $controller('editor', {$rootScope: rootScope, $scope: scope, history: history, dialog: dialog, constructor: constructor, linker: linker, functions: functions});
        });
    });
    it('copies the history\'s canUndo to the scope', function(){
        expect(scope.canUndo).toBe(historyCanUndo);
    });
    it('copies the history\'s canRedo to the scope', function(){
        expect(scope.canRedo).toBe(historyCanRedo);
    });    
    it('copies the history\'s canUndo to the scope', function(){
        expect(scope.canUndo).toBe(historyCanUndo);
    });
    it('copies the history\'s canRedo to the scope', function(){
        expect(scope.canRedo).toBe(historyCanRedo);
    });    
    it('does not call any of these functions', function(){
        expect(historyCanUndo).not.toHaveBeenCalled();
        expect(historyCanRedo).not.toHaveBeenCalled();
        expect(historyUndo).not.toHaveBeenCalled();
        expect(historyRedo).not.toHaveBeenCalled();
        expect(historyClear).not.toHaveBeenCalled();
    });
    it('copies the dialog to the scope', function(){
        expect(scope.dialog).toBe(dialog);
    });
    it('creates a default node', function(){
        expect(constructor).toHaveBeenCalledWith('sceneVolumetric');
        expect(rootScope.node).toEqual('Test Node');
    })
    it('provides a function for getting the linked code', function(){
        scope.node = 'Test Node';
        var result = scope.generatedCode();
        expect(linker).toHaveBeenCalledWith('Test Node');
        expect(result).toEqual('Test Linked Code');
    });
    describe('new', function(){
        beforeEach(function(){
            dialog.show = jasmine.createSpy();
            scope.node = rootScope.node = 'Old Node';
            constructor.and.returnValue('New Node');
            constructor.calls.reset();
            scope.new();
        });
        it('does not clear the history yet', function(){
            expect(historyClear).not.toHaveBeenCalled();
        });
        it('does not replace the node yet', function(){
            expect(rootScope.node).toEqual('Old Node');
        });
        it('shows a dialog confirming the user wishes to make a new scene', function(){
            expect(dialog.show).toHaveBeenCalled();
            expect(dialog.show.calls.argsFor(0)[0]).toEqual('areYouSureNew');
            
            expect(Object.keys(dialog.show.calls.argsFor(0)[1]).length).toEqual(1);
            expect(dialog.show.calls.argsFor(0)[1].ok).toBeDefined()
        });
        describe('on clicking ok', function(){
            beforeEach(function(){
                dialog.show.calls.argsFor(0)[1].ok();
            });
            it('clears the history', function(){
                expect(historyClear).toHaveBeenCalled();
            });
            it('calls the constructor', function(){
                expect(constructor).toHaveBeenCalledWith('sceneVolumetric');
            });
            it('copies the new node to the root scope', function(){
                expect(rootScope.node).toEqual('New Node');
            });
        });
    });
    describe('browser', function(){
        it('defines a browser object', function(){
            expect(scope.browser).toBeDefined();
        });
        it('is not replacing anything', function(){
            expect(scope.browser.replacing).toBeFalsy();
        });
        it('is not adding to anything', function(){
            expect(scope.browser.addingTo).toBeFalsy();
        });   
        describe('beginAddingTo', function(){
            var node;
            beforeEach(function(){
                node = {};
                scope.browser.addingTo = {};
                scope.browser.replacing = {};
                scope.browser.beginAddingTo(node);
            });
            it('does not modify the node', function(){
                expect(node).toEqual({});
            });
            it('sets replacing to null', function(){
                expect(scope.browser.replacing).toBeFalsy();
            });
            it('copies the node to addingTo', function(){
                expect(scope.browser.addingTo).toBe(node);
            });
        });
        describe('beginReplacing', function(){
            var node;
            beforeEach(function(){
                node = {};
                scope.browser.addingTo = {};
                scope.browser.replacing = {};
                scope.browser.beginReplacing(node);
            });
            it('does not modify the node', function(){
                expect(node).toEqual({});
            });
            it('sets addingTo to null', function(){
                expect(scope.browser.addingTo).toBeFalsy();
            });
            it('copies the node to replacing', function(){
                expect(scope.browser.replacing).toBe(node);
            });
        });  
        it('copies the function list', function(){
            expect(scope.browser.options).toEqual(['Test Function A', 'Test Function B', 'Test Function C']);
        });
    });
});