angular.module('luminous-composer').controller('node', function($scope, $rootScope, history, operations){
    $scope.replace = function(){
        $scope.browser.beginReplacing($scope.node);
    };
    
    $scope.add = function(){
        $scope.browser.beginAddingTo($scope.node);
    };    
    
    $scope.remove = function(){
        var parent = operations.findParent($rootScope.node, $scope.node);
        var index = parent.items.indexOf($scope.node);
        history.act(function(){
            parent.items.splice(index, 1);
        }, function(){
            parent.items.splice(index, 0, $scope.node);
        });
    };    
    
    $scope.browserTitle = function(){
        if($scope.browser.addingTo == $scope.node) return 'browserTitleAddingTo';
        if($scope.browser.replacing == $scope.node) return 'browserTitleReplacing';
        return null;
    };
    
    $scope.persist = function(){
        if($scope.node.value == $scope.node.historyValue) return;
        
        var newValue = $scope.node.value;
        var oldValue = $scope.node.historyValue;
        
        $scope.node.value = $scope.node.historyValue;
        
        history.act(function(){
            $scope.node.value = $scope.node.historyValue = newValue;
        }, function(){
            $scope.node.value = $scope.node.historyValue = oldValue;
        });
    };
});