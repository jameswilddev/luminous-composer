angular.module('luminous-composer').value('functions', {
    float: {
        returns: 'float',
        defaultValue: 0.5
    },
    sceneVolumetric: {
        returns: 'scene',
        fields: {
            lens: 'lens',
            lights: 'volumetricSum'
        },
        fieldOrder: ['lens', 'lights']
    },
    lens: {
        returns: 'ray',
        fields: {
            fieldOfView: 'float'
        },
        fieldOrder: ['fieldOfView']
    },
    volumetricSum: {
        returns: 'volumetric',
        arrayOf: 'volumetric'
    },
    occluderSum: {
        returns: 'occluder',
        arrayOf: 'occluder'
    }
});