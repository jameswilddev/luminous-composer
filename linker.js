angular.module('luminous-composer').value('fragmentHeader', [
    '#ifdef GL_ES',
    '   precision mediump float;',
    '#endif',    
    'uniform vec2 resolution;',
    '#define sceneVolumetric(lens, volumetrics) lens',
    '// Returns the location of the current fragment relative to the center of the screen, where 0.5 is the distance to the nearest screen border.',
    '// This will return values > +-0.5 on the X axis in widescreen, and the Y axis in portrait.',
    'vec2 pixelCoord() { return ((gl_FragCoord.xy - (resolution / 2.0)) / min(resolution.x, resolution.y)); }',
    'vec3 lens(float fieldOfView){ return normalize(vec3(pixelCoord(), fieldOfView)); }',
    'void main() {',
    '   gl_FragColor = vec4(pow('
]).value('fragmentFooter', [
    ', vec3(1.0)), 1.0);',
    '}'
]).service('linker', function(compiler, fragmentHeader, fragmentFooter){
    return function(node) {
        return fragmentHeader.join('\n') + compiler(node) + fragmentFooter.join('\n');
    };
});