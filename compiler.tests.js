describe('compiler', function(){
    var run = function(node, _functions){
        var result;
        module('luminous-composer');
        inject(function(compiler, functions){
            // We can't mock an AngularJS constant for some reason, so instead
            // all we can do is replace its contents.
            var toRemove = [];
            angular.forEach(functions, function(unused, key){
                toRemove.push(key);
            });
            angular.forEach(toRemove, function(key){
                delete functions[key];
            });
            angular.copy(_functions, functions);
            
            result = compiler(node);
        });
        return result;
    };
    describe('primitive type', function(){
        describe('float', function(){
            it('converts to a string', function(){
                expect(run({
                    type: "float",
                    value: 3.6
                })).toEqual('3.6');
            });
            it('supports negative', function(){
                expect(run({
                    type: "float",
                    value: -3.6
                })).toEqual('-3.6');
            });    
            it('supports zero', function(){
                expect(run({
                    type: "float",
                    value: 0.0
                })).toEqual('0.0');
            });              
            it('always includes decimal places', function(){
                expect(run({
                    type: "float",
                    value: 4
                })).toEqual('4.0');                
            });
            it('always includes decimal places for negative', function(){
                expect(run({
                    type: "float",
                    value: -4
                })).toEqual('-4.0');                
            });            
            it('always includes decimal places for zero', function(){
                expect(run({
                    type: "float",
                    value: 0
                })).toEqual('0.0');                
            });   
        });
        describe('booleans', function(){
            it('outputs true as true', function(){
                expect(run({
                    type: "pattern",
                    value: true
                }, {
                    pattern: {}
                })).toEqual('true');                
            });
            it('outputs false as false', function(){
                expect(run({
                    type: "pattern",
                    value: false
                }, {
                    pattern: {}
                })).toEqual('false');                
            });            
        });
    });
    describe('with fields', function(){
        it('supports function calls without arguments', function(){
            expect(run({
                type: "callB"
            }, {
                callA: {
                    fieldOrder: ["fieldB", "fieldA", "fieldC"]
                },
                callB: {
                    fieldOrder: []
                },
                callC: {
                    fieldOrder: []
                }                
            })).toEqual('callB()');     
        });
        it('compiles to a function call, respecting the order set in fieldOrder', function(){
            expect(run({
                type: "callB",
                fields: {
                    fieldA: {
                        type: "float",
                        value: 2.3
                    },
                    fieldB: {
                        type: "float",
                        value: 4.6
                    },
                    fieldC: {
                        type: "float",
                        value: 7.1
                    }
                }
            }, {
                callA: {
                    fieldOrder: ["fieldB", "fieldA", "fieldC"]
                },
                callB: {
                    fieldOrder: ["fieldB", "fieldC", "fieldA"]
                },
                callC: {
                    fieldOrder: []
                }
            })).toEqual('callB(4.6, 7.1, 2.3)');     
        });
        it('can contain arrays', function(){
            expect(run({
                type: "callB",
                fields: {
                    fieldA: {
                        type: "float",
                        value: 2.3
                    },
                    fieldB: {
                        type: "float",
                        value: 4.6
                    },
                    fieldC: {
                        type: "arrayType",
                        items: [{
                            type: "float",
                            value: 6.3
                        }, {
                            type: "float",
                            value: -4.7
                        }]
                    }
                }
            }, {
                callA: {
                    fieldOrder: ["fieldB", "fieldA", "fieldC"]
                },
                callB: {
                    fieldOrder: ["fieldB", "fieldC", "fieldA"]
                },
                callC: {
                    fieldOrder: []
                },
                arrayType: {
                    whenEmpty: "???"
                }
            })).toEqual('callB(4.6, arrayType(6.3, -4.7), 2.3)');   
        });
        it('can contain functions', function(){
            expect(run({
                type: "callB",
                fields: {
                    fieldA: {
                        type: "float",
                        value: 2.3
                    },
                    fieldB: {
                        type: "float",
                        value: 4.6
                    },
                    fieldC: {
                        type: "callA",
                        fields: {
                            fieldA: {
                                type: "float",
                                value: 8.3
                            },
                            fieldB: {
                                type: "float",
                                value: -33.4
                            },
                            fieldC: {
                                type: "float",
                                value: 2.9
                            }
                        }
                    }
                }
            }, {
                callA: {
                    fieldOrder: ["fieldB", "fieldA", "fieldC"]
                },
                callB: {
                    fieldOrder: ["fieldB", "fieldC", "fieldA"]
                },
                callC: {
                    fieldOrder: []
                }
            })).toEqual('callB(4.6, callA(-33.4, 8.3, 2.9), 2.3)');   
        });
    });
    describe('arrays', function(){
        it('returns whenEmpty when no items are present', function(){
            expect(run({
                type: "arrayB",
                items: []
            }, {
                arrayA: {
                    whenEmpty: "a empty"
                },
                arrayB: {
                    whenEmpty: "b empty"
                },
                arrayC: {
                    whenEmpty: "c empty"
                }
            })).toEqual('b empty');    
        });
        it('returns the first item when only one is present', function(){
            expect(run({
                type: "arrayB",
                items: [{
                    type: "float",
                    value: 4.6
                }]
            }, {
                arrayA: {
                    whenEmpty: "a empty"
                },
                arrayB: {
                    whenEmpty: "b empty"
                },
                arrayC: {
                    whenEmpty: "c empty"
                }
            })).toEqual('4.6');   
        });
        it('makes recursive calls to aggregate the items inside', function(){
            expect(run({
                type: "arrayB",
                items: [{
                    type: "float",
                    value: 4.6
                }, {
                    type: "float",
                    value: -6.7                    
                }, {
                    type: "float",
                    value: 8.9                    
                }]
            }, {
                arrayA: {
                    whenEmpty: "a empty"
                },
                arrayB: {
                    whenEmpty: "b empty"
                },
                arrayC: {
                    whenEmpty: "c empty"
                }
            })).toEqual('arrayB(arrayB(4.6, -6.7), 8.9)');  
        });
        it('can contain functions', function(){
            expect(run({
                type: "arrayB",
                items: [{
                    type: "float",
                    value: 4.6
                }, {
                    type: "callB",
                    fields: {
                        fieldA: {
                            type: "float",
                            value: 8.1
                        },
                        fieldB: {
                            type: "float",
                            value: 4.1
                        },
                        fieldC: {
                            type: "float",
                            value: 9.3
                        }
                    }                   
                }, {
                    type: "float",
                    value: 8.9                    
                }]
            }, {
                arrayA: {
                    whenEmpty: "a empty"
                },
                arrayB: {
                    whenEmpty: "b empty"
                },
                arrayC: {
                    whenEmpty: "c empty"
                },
                callA: {
                    fieldOrder: ["fieldB", "fieldA", "fieldC"]
                },
                callB: {
                    fieldOrder: ["fieldB", "fieldC", "fieldA"]
                },
                callC: {
                    fieldOrder: []
                }                
            })).toEqual('arrayB(arrayB(4.6, callB(4.1, 9.3, 8.1)), 8.9)');  
        });
        it('can contain arrays', function(){
            expect(run({
                type: "arrayB",
                items: [{
                    type: "float",
                    value: 4.6
                }, {
                    type: "arrayC",
                    items: [{
                        type: "float",
                        value: 7.4
                    }, {
                        type: "float",
                        value: 1.3
                    }, {
                        type: "float",
                        value: 4.5
                    }]            
                }, {
                    type: "float",
                    value: 8.9                    
                }]
            }, {
                arrayA: {
                    whenEmpty: "a empty"
                },
                arrayB: {
                    whenEmpty: "b empty"
                },
                arrayC: {
                    whenEmpty: "c empty"
                }             
            })).toEqual('arrayB(arrayB(4.6, arrayC(arrayC(7.4, 1.3), 4.5)), 8.9)');  
        });        
    });
});