describe('browserItem', function(){
    var scope, rootScope, browser, node, functions, history, operations, constructor;
    beforeEach(function(){
        scope = {
            node: node = {},
            option: 'Test Function C',
            browser: browser = {
                replacing: null,
                addingTo: null
            },
            history: history = {},
            operations: operations = {}
        };
        rootScope = {};
        functions = {
            'Test Function A': {},
            'Test Function B': {},
            'Test Function C': {},
            'Test Function D': {}
        };
        constructor = jasmine.createSpy();
        module('luminous-composer');
        inject(function($controller){
            $controller('browserItem', {$scope: scope, $rootScope: rootScope, functions: functions, history: history, operations: operations, constructor: constructor});
        });
    });
    describe('when replacing', function(){
        beforeEach(function(){
            browser.replacing = node;
        });
        describe('available', function(){
            it('returns true when the return type of this browser item matches the return type of the node being replaced', function(){
                functions['Test Function A'].returns = 'Test Return A';
                functions['Test Function B'].returns = 'Test Return B';
                functions['Test Function C'].returns = 'Test Return B';
                functions['Test Function D'].returns = 'Test Return D';
                node.type = 'Test Function B';
                expect(scope.available()).toBeTruthy();
            });
            it('returns false when the return type of this browser item does not match the return type of the node being replaced', function(){
                functions['Test Function A'].returns = 'Test Return A';
                functions['Test Function B'].returns = 'Test Return B';
                functions['Test Function C'].returns = 'Test Return C';
                functions['Test Function D'].returns = 'Test Return B';
                node.type = 'Test Function B';
                expect(scope.available()).toBeFalsy();
            });
            it('returns false even if the node is an array of this browser item', function(){
                functions['Test Function A'].arrayOf = 'Test Return A';
                functions['Test Function A'].returns = 'Test Return X';
                functions['Test Function B'].arrayOf = 'Test Return B';
                functions['Test Function B'].returns = 'Test Return Y';
                functions['Test Function C'].arrayOf = 'Test Return C';
                functions['Test Function C'].returns = 'Test Return Z';
                functions['Test Function D'].arrayOf = 'Test Return B';
                functions['Test Function D'].returns = 'Test Return W';
                node.type = 'Test Function B';
                expect(scope.available()).toBeFalsy();
            });            
        });
        describe('use', function(){
            var instance;
            beforeEach(function(){
                node.arrayItem = 'Test ArrayItem Value';
                history.act = jasmine.createSpy();
                constructor.and.returnValue(instance = {});
                scope.use();
            });
            it('creates a new instance of the function', function(){
                expect(constructor).toHaveBeenCalledWith('Test Function C');
            });
            it('ends replacing the node', function(){
                expect(browser.replacing).toBeFalsy();
            });
            it('copies arrayItem to the constructed instance', function(){
                expect(instance.arrayItem).toEqual('Test ArrayItem Value');
            })            
            it('registers a new history step', function(){
                expect(history.act).toHaveBeenCalled();
            });
            describe('on performing the history step', function(){
                beforeEach(function(){
                    operations.replace = jasmine.createSpy();
                    history.act.calls.argsFor(0)[0]();
                });
                it('replaces the existing node with the newly created node', function(){
                    expect(operations.replace.calls.count()).toEqual(1);
                    expect(operations.replace.calls.argsFor(0)[0]).toBe(rootScope);
                    expect(operations.replace.calls.argsFor(0)[1]).toBe(node);
                    expect(operations.replace.calls.argsFor(0)[2]).toBe(instance);
                });
                describe('on subsequent calls', function(){
                    beforeEach(function(){
                        history.act.calls.argsFor(0)[0]();
                    })
                    it('does not call compiler again', function(){
                        expect(constructor.calls.count()).toEqual(1);
                    });
                });
            });
            describe('on undoing the history step', function(){
                beforeEach(function(){
                    operations.replace = jasmine.createSpy();
                    history.act.calls.argsFor(0)[1]();
                });
                it('replaces the newly created node with the existing node', function(){
                    expect(operations.replace.calls.count()).toEqual(1);
                    expect(operations.replace.calls.argsFor(0)[0]).toBe(rootScope);
                    expect(operations.replace.calls.argsFor(0)[1]).toBe(instance);
                    expect(operations.replace.calls.argsFor(0)[2]).toBe(node);
                });
                describe('on subsequent calls', function(){
                    beforeEach(function(){
                        history.act.calls.argsFor(0)[1]();
                    })
                    it('does not call compiler again', function(){
                        expect(constructor.calls.count()).toEqual(1);
                    });
                });
            });            
        });
    });
    describe('when adding to', function(){
        beforeEach(function(){
            browser.addingTo = node;
        });
        describe('available', function(){
            it('returns true when the return type of this browser item matches the type the node being replaced is an array of', function(){
                functions['Test Function A'].arrayOf = 'Test Return A';
                functions['Test Function A'].returns = 'Test Return X';
                functions['Test Function B'].arrayOf = 'Test Return B';
                functions['Test Function B'].returns = 'Test Return Y';
                functions['Test Function C'].arrayOf = 'Test Return C';
                functions['Test Function C'].returns = 'Test Return B';
                functions['Test Function D'].arrayOf = 'Test Return D';
                functions['Test Function D'].returns = 'Test Return W';                
                
                node.type = 'Test Function B';
                expect(scope.available()).toBeTruthy();
            });
            it('returns false when the return type of this browser item does not match the return type of the node being replaced', function(){
                functions['Test Function A'].arrayOf = 'Test Return A';
                functions['Test Function A'].returns = 'Test Return X';
                functions['Test Function B'].arrayOf = 'Test Return B';
                functions['Test Function B'].returns = 'Test Return Y';
                functions['Test Function C'].arrayOf = 'Test Return C';
                functions['Test Function C'].returns = 'Test Return Z';
                functions['Test Function D'].arrayOf = 'Test Return D';
                functions['Test Function D'].returns = 'Test Return B';    
                
                node.type = 'Test Function B';
                expect(scope.available()).toBeFalsy();
            });
            it('returns false when the return type of this browser item matches the return type of the node being replaced', function(){
                functions['Test Function A'].arrayOf = 'Test Return A';
                functions['Test Function A'].returns = 'Test Return X';
                functions['Test Function B'].arrayOf = 'Test Return B';
                functions['Test Function B'].returns = 'Test Return Y';
                functions['Test Function C'].arrayOf = 'Test Return C';
                functions['Test Function C'].returns = 'Test Return Y';
                functions['Test Function D'].arrayOf = 'Test Return D';
                functions['Test Function D'].returns = 'Test Return W';    
                node.type = 'Test Function B';
                expect(scope.available()).toBeFalsy();
            });            
        });
        describe('use', function(){
            var instance;
            beforeEach(function(){
                history.act = jasmine.createSpy();
                constructor.and.returnValue(instance = {});
                scope.use();
            });
            it('creates a new instance of the function', function(){
                expect(constructor).toHaveBeenCalledWith('Test Function C');
            });
            it('ends adding to the node', function(){
                expect(browser.addingTo).toBeFalsy();
            });
            it('sets arrayItem to true in the constructed instance', function(){
                expect(instance.arrayItem).toBeTruthy();
            })
            it('registers a new history step', function(){
                expect(history.act).toHaveBeenCalled();
            });
            describe('on performing the history step', function(){
                beforeEach(function(){
                    node.items = [
                        'Ignored A',
                        'Ignored B'
                    ];                    
                    history.act.calls.argsFor(0)[0]();
                });
                it('adds the new instance to the end of the node\'s items', function(){
                    expect(node.items.length).toEqual(3);
                    expect(node.items[0]).toEqual('Ignored A');
                    expect(node.items[1]).toEqual('Ignored B');
                    expect(node.items[2]).toBe(instance);
                });
                describe('on subsequent calls', function(){
                    beforeEach(function(){
                        history.act.calls.argsFor(0)[0]();
                    })
                    it('does not call compiler again', function(){
                        expect(constructor.calls.count()).toEqual(1);
                    });
                });
            });
            describe('on undoing the history step', function(){
                beforeEach(function(){
                    node.items = [
                        'Ignored A',
                        'Ignored B',
                        instance
                    ];
                    history.act.calls.argsFor(0)[1]();
                });
                it('removes the new instance from the end of the node\'s items', function(){
                    expect(node.items).toEqual([
                        'Ignored A',
                        'Ignored B'
                    ]);
                });
                describe('on subsequent calls', function(){
                    beforeEach(function(){
                        history.act.calls.argsFor(0)[1]();
                    })
                    it('does not call compiler again', function(){
                        expect(constructor.calls.count()).toEqual(1);
                    });
                });
            });            
        });
    });    
});