describe('linker', function(){
    var compiler, result;
    beforeEach(function(){
        compiler = jasmine.createSpy();
        compiler.and.returnValue('Test Compiled Code');
        module('luminous-composer', function($provide){
            $provide.value('fragmentHeader', ['Test Header Line A', 'Test Header Line B']);
            $provide.value('fragmentFooter', ['Test Footer Line A', 'Test Footer Line B']);
            $provide.service('compiler', function(){ return compiler; });
        });
        inject(function(linker){
            result = linker('Test Node');
        });
    });
    it('compiles the given node', function(){
        expect(compiler).toHaveBeenCalledWith('Test Node');
    });
    it('builds the header, compiled node and footer into a single piece of code', function(){
        expect(result).toEqual('Test Header Line A\nTest Header Line BTest Compiled CodeTest Footer Line A\nTest Footer Line B');
    });
});