describe('dialog', function(){
    var service;
    beforeEach(function(){
        module('luminous-composer');
        inject(function(dialog){
            service = dialog;
        });
    })
    describe('initial state', function(){
        it('has no message', function(){
            expect(service.message).toBeFalsy();
        });
        it('has no details', function(){
            expect(service.details).toBeFalsy();
        });        
        it('has no buttons', function(){
            expect(service.buttons).toBeFalsy();
        });
    });
    describe('on calling show', function(){
        it('copies the message', function(){
            service.show('Test Message');
            expect(service.message).toEqual('Test Message');
        });
        it('has no details', function(){
            service.details = {};
            service.show();
            expect(service.details).toBeFalsy();
        });       
        it('populates its buttons', function(){
            service.show(undefined, {
                testButtonA: null,
                testButtonB: null
            });
            expect(service.buttons.testButtonA).toBeDefined();
            expect(service.buttons.testButtonB).toBeDefined();
        });     
        describe('on clicking a button', function(){
            var spyA, spyB;
            beforeEach(function(){
                spyA = jasmine.createSpy();
                spyB = jasmine.createSpy();
                service.show('Test Message', {
                    testButtonA: spyA,
                    testButtonB: spyB
                });
                service.buttons.testButtonB();
            });
            it('calls the button\'s callback', function(){
                expect(spyB).toHaveBeenCalled();
            });
            it('does not call other buttons\' callbacks', function(){
                expect(spyA).not.toHaveBeenCalled();
            });
            it('has no message', function(){
                expect(service.message).toBeFalsy();
            });
            it('has no details', function(){
                expect(service.details).toBeFalsy();
            });       
            it('has no buttons', function(){
                expect(service.buttons).toBeFalsy();
            });
        });
        describe('on calling close', function(){
            beforeEach(function(){
                service.show('Test Message', {
                    testButtonA: null,
                    testButtonB: null
                });
                service.close();
            });
            it('has no message', function(){
                expect(service.message).toBeFalsy();
            });
            it('has no details', function(){
                expect(service.details).toBeFalsy();
            });               
            it('has no buttons', function(){
                expect(service.buttons).toBeFalsy();
            });
        });
    });
    describe('on calling showDetails', function(){
        it('copies the message', function(){
            service.showDetails('Test Message');
            expect(service.message).toEqual('Test Message');
        });
        it('copies the details', function(){
            service.showDetails(undefined, 'Test Details');
            expect(service.details).toEqual('Test Details');
        });            
        it('leaves buttons null', function(){
            service.buttons = {};
            service.showDetails('Test Message');
            expect(service.buttons).toBeFalsy();
        });     
        describe('on calling close', function(){
            beforeEach(function(){
                service.showDetails('Test Message', 'Test Details');
                service.close();
            });
            it('has no message', function(){
                expect(service.message).toBeFalsy();
            });
            it('has no details', function(){
                expect(service.details).toBeFalsy();
            });               
            it('has no buttons', function(){
                expect(service.buttons).toBeFalsy();
            });
        });
    });     
    describe('on calling showError', function(){
        it('copies the message', function(){
            service.showError('Test Message');
            expect(service.message).toEqual('Test Message');
        });
        it('leaves details null', function(){
            service.details = {};
            service.showError();
            expect(service.details).toBeFalsy();
        });             
        it('leaves buttons null', function(){
            service.buttons = {};
            service.showError('Test Message');
            expect(service.buttons).toBeFalsy();
        });     
        describe('on calling close', function(){
            beforeEach(function(){
            service.showError('Test Message');
            expect(service.message).toEqual('Test Message');
                service.close();
            });
            it('the message remains', function(){
                expect(service.message).toEqual('Test Message');
            });
        });
    });    
});