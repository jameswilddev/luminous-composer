angular.module('luminous-composer').service('dialog', function(){
    var service = {
        show: function(message, buttons) {
            service.message = message;
            service.details = null;
            service.buttons = {};
            
            angular.forEach(buttons, function(value, key){
                service.buttons[key] = function(){
                    service.close();
                    value();
                };
            });
        },
        showDetails: function(message, details) {
            service.message = message;
            service.details = details;
            service.buttons = null;
        },        
        showError: function(message) {
            service.message = message;
            service.details = null;
            service.buttons = null;
        },
        close: function() {
            if(!service.buttons && !service.details) return;
            service.message = service.buttons = service.details = null;
        },
        message: null,
        details: null,
        buttons: null
    };
    
    return service;
});