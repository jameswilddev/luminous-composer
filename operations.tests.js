describe('operations', function(){
    var service;
    beforeEach(function(){
        module('luminous-composer');
        inject(function(operations){
            service = operations;
        });
    });
    describe('replace', function(){
        it('replaces the specified node when it is the root', function(){
            var nodeContainer = {
                node: 'Test To Replace'
            };
            
            service.replace(nodeContainer, 'Test To Replace', 'Test Replace With');
            
            expect(nodeContainer).toEqual({
                node: 'Test Replace With'
            });
        });
        it('replaces the node with the constructed instance when it is a field', function(){
            var nodeContainer = {
                node: {
                    fields: {
                        a: 'Miscellaneous Field A A',
                        b: {
                            fields: {
                                a: 'Miscellaneous Field B A',
                                b: 'Test To Replace',
                                c: 'Miscellaneous Field B C'
                            }
                        },
                        c: 'Miscellaneous Field A C'
                    }
                }
            };
            
            service.replace(nodeContainer, 'Test To Replace', 'Test Replace With');
            
            expect(nodeContainer).toEqual({
                node: {
                    fields: {
                        a: 'Miscellaneous Field A A',
                        b: {
                            fields: {
                                a: 'Miscellaneous Field B A',
                                b: 'Test Replace With',
                                c: 'Miscellaneous Field B C'
                            }
                        },
                        c: 'Miscellaneous Field A C'
                    }
                }
            });                            
        });
        it('replaces the node with the constructed instance when it is an array item', function(){
            var nodeContainer = {
                node: {
                    fields: {
                        a: 'Miscellaneous Field A A',
                        b: {
                            fields: {
                                a: 'Miscellaneous Field B A',
                                b: 'Test To Replace',
                                c: 'Miscellaneous Field B C'
                            }
                        },
                        c: 'Miscellaneous Field A C'
                    }
                }
            };
            
            service.replace(nodeContainer, 'Test To Replace', 'Test Replace With');
            
            expect(nodeContainer).toEqual({
                node: {
                    fields: {
                        a: 'Miscellaneous Field A A',
                        b: {
                            fields: {
                                a: 'Miscellaneous Field B A',
                                b: 'Test Replace With',
                                c: 'Miscellaneous Field B C'
                            }
                        },
                        c: 'Miscellaneous Field A C'
                    }
                }
            });
        }); 
    });
    describe('findParent', function(){
        var root = {
            fields: {
                a: {
                    items: [
                        'Test Array Item A',
                        'Test Array Item B',
                        'Test Array Item C'
                    ]
                },
                b: {
                    fields: {
                        ba: 'Test Field A',
                        bb: 'Test Field B',
                        bc: 'Test Field C'
                    }
                }
            }
        };        
        it('returns null when the node is the root', function(){
            expect(service.findParent(root, root)).toBeFalsy();
        });
        it('returns the containing array when the parent is an array', function(){
            expect(service.findParent(root, 'Test Array Item B')).toBe(root.fields.a);
        });
        it('returns the containing field set when the parent is a field set', function(){
            expect(service.findParent(root, 'Test Field B')).toBe(root.fields.b);
        });
    });
});