describe('history', function(){
    var dialog, service;
    beforeEach(function(){
        dialog = {};
        module('luminous-composer', function($provide){
            $provide.value('dialog', dialog);
        });
        inject(function(history){
            service = history;
        });
    });
    describe('initial state', function(){
        it('does not allow undo', function(){
            expect(service.canUndo()).toBeFalsy();
        });
        it('does not allow redo', function(){
            expect(service.canRedo()).toBeFalsy();
        });
    });
    describe('clearing', function(){
        beforeEach(function(){
            service.clear();
        });
        it('does not allow undo', function(){
            expect(service.canUndo()).toBeFalsy();
        });
        it('does not allow redo', function(){
            expect(service.canRedo()).toBeFalsy();
        });
        describe('on performing an action', function(){
            var clearDo, clearUndo;
            beforeEach(function(){
                clearDo = jasmine.createSpy();
                clearUndo = jasmine.createSpy();
                service.act(clearDo, clearUndo);
            });
            it('allows undo', function(){
                expect(service.canUndo()).toBeTruthy();
            });
            it('does not allow redo', function(){
                expect(service.canRedo()).toBeFalsy();
            });        
            it('executes the do callback', function(){
                expect(clearDo).toHaveBeenCalled();
            });
            it('does not execute the undo callback', function(){
                expect(clearUndo).not.toHaveBeenCalled();
            });   
            describe('on undoing that action', function(){
                beforeEach(function(){
                    clearDo.calls.reset();
                    clearUndo.calls.reset();
                    service.undo();
                });
                it('allows redo', function(){
                    expect(service.canRedo()).toBeTruthy();
                });
                it('does not allow undo', function(){
                    expect(service.canUndo()).toBeFalsy();
                });        
                it('executes the undo callback', function(){
                    expect(clearUndo).toHaveBeenCalled();
                });
                it('does not execute the do callback', function(){
                    expect(clearDo).not.toHaveBeenCalled();
                });         
                describe('on redoing that action', function(){
                    beforeEach(function(){
                        clearDo.calls.reset();
                        clearUndo.calls.reset();
                        service.redo();
                    });
                    it('allows undo', function(){
                        expect(service.canUndo()).toBeTruthy();
                    });
                    it('does not allow redo', function(){
                        expect(service.canRedo()).toBeFalsy();
                    });        
                    it('executes the do callback', function(){
                        expect(clearDo).toHaveBeenCalled();
                    });
                    it('does not execute the undo callback', function(){
                        expect(clearUndo).not.toHaveBeenCalled();
                    });  
                });                  
            });
        });
    });
    describe('on performing an action', function(){
        var aDo, aUndo;
        beforeEach(function(){
            aDo = jasmine.createSpy();
            aUndo = jasmine.createSpy();
            service.act(aDo, aUndo);
        });
        it('allows undo', function(){
            expect(service.canUndo()).toBeTruthy();
        });
        it('does not allow redo', function(){
            expect(service.canRedo()).toBeFalsy();
        });        
        it('executes the do callback', function(){
            expect(aDo).toHaveBeenCalled();
        });
        it('does not execute the undo callback', function(){
            expect(aUndo).not.toHaveBeenCalled();
        });   
        describe('on undoing that action', function(){
            beforeEach(function(){
                aDo.calls.reset();
                aUndo.calls.reset();
                service.undo();
            });
            it('allows redo', function(){
                expect(service.canRedo()).toBeTruthy();
            });
            it('does not allow undo', function(){
                expect(service.canUndo()).toBeFalsy();
            });        
            it('executes the undo callback', function(){
                expect(aUndo).toHaveBeenCalled();
            });
            it('does not execute the do callback', function(){
                expect(aDo).not.toHaveBeenCalled();
            });  
            describe('on performing a new action', function(){
                var bDo, bUndo;
                beforeEach(function(){
                    aDo.calls.reset();
                    aUndo.calls.reset();
                    bDo = jasmine.createSpy();
                    bUndo = jasmine.createSpy();
                    dialog.show = jasmine.createSpy();
                    service.act(bDo, bUndo);
                });
                describe('the action is deferred', function(){
                    it('allows redo', function(){
                        expect(service.canRedo()).toBeTruthy();
                    });
                    it('does not allow undo', function(){
                        expect(service.canUndo()).toBeFalsy();
                    });        
                    it('does not execute the old callbacks', function(){
                        expect(aDo).not.toHaveBeenCalled();
                        expect(aUndo).not.toHaveBeenCalled();
                    });       
                    it('does not execute the new callbacks', function(){
                        expect(bDo).not.toHaveBeenCalled();
                        expect(bUndo).not.toHaveBeenCalled();
                    });         
                    it('shows a dialog warning to the user', function(){
                        expect(dialog.show).toHaveBeenCalled();
                        expect(dialog.show.calls.argsFor(0)[0]).toEqual('undoneChangesWillBeErased');
                        expect(dialog.show.calls.argsFor(0)[1].ok).toBeDefined();
                    });
                    describe('on clicking ok', function(){
                        beforeEach(function(){
                            var ok = dialog.show.calls.argsFor(0)[1].ok;
                            aDo.calls.reset();
                            aUndo.calls.reset();
                            bDo.calls.reset();
                            bUndo.calls.reset();   
                            dialog.show.calls.reset();
                            ok();
                        });
                        it('allows undo', function(){
                            expect(service.canUndo()).toBeTruthy();
                        });
                        it('does not allow redo', function(){
                            expect(service.canRedo()).toBeFalsy();
                        });        
                        it('executes the do callback', function(){
                            expect(bDo).toHaveBeenCalled();
                        });
                        it('does not execute the undo callback', function(){
                            expect(bUndo).not.toHaveBeenCalled();
                        });      
                        it('does not execute the old callbacks', function(){
                            expect(aDo).not.toHaveBeenCalled();
                            expect(aUndo).not.toHaveBeenCalled();
                        }); 
                        describe('on undoing', function(){
                            beforeEach(function(){
                                aDo.calls.reset();
                                aUndo.calls.reset();
                                bDo.calls.reset();
                                bUndo.calls.reset();   
                                dialog.show = undefined;
                                service.undo();
                            });
                            it('allows redo', function(){
                                expect(service.canRedo()).toBeTruthy();
                            });
                            it('does not allow undo', function(){
                                expect(service.canUndo()).toBeFalsy();
                            });        
                            it('executes the undo callback', function(){
                                expect(bUndo).toHaveBeenCalled();
                            });
                            it('does not execute the do callback', function(){
                                expect(bDo).not.toHaveBeenCalled();
                            });      
                            it('does not execute the old callbacks', function(){
                                expect(aDo).not.toHaveBeenCalled();
                                expect(aUndo).not.toHaveBeenCalled();
                            });                             
                            describe('on redoing', function(){
                                beforeEach(function(){
                                    aDo.calls.reset();
                                    aUndo.calls.reset();
                                    bDo.calls.reset();
                                    bUndo.calls.reset();   
                                    dialog.show = undefined;
                                    service.redo();
                                });
                                it('allows undo', function(){
                                    expect(service.canUndo()).toBeTruthy();
                                });
                                it('does not allow redo', function(){
                                    expect(service.canRedo()).toBeFalsy();
                                });        
                                it('executes the undo callback', function(){
                                    expect(bDo).toHaveBeenCalled();
                                });
                                it('does not execute the do callback', function(){
                                    expect(bUndo).not.toHaveBeenCalled();
                                });      
                                it('does not execute the old callbacks', function(){
                                    expect(aDo).not.toHaveBeenCalled();
                                    expect(aUndo).not.toHaveBeenCalled();
                                }); 
                            });
                        });
                    });
                    describe('on cancelling', function(){
                        describe('on performing a new action', function(){
                            var cDo, cUndo;
                            beforeEach(function(){
                                aDo.calls.reset();
                                aUndo.calls.reset();
                                bDo.calls.reset();
                                bUndo.calls.reset();   
                                dialog.show.calls.reset();
                                cDo = jasmine.createSpy();
                                cUndo = jasmine.createSpy();
                                service.act(cDo, cUndo);
                            });
                            describe('the action is deferred', function(){
                                it('allows redo', function(){
                                    expect(service.canRedo()).toBeTruthy();
                                });
                                it('does not allow undo', function(){
                                    expect(service.canUndo()).toBeFalsy();
                                });        
                                it('does not execute the old callbacks', function(){
                                    expect(aDo).not.toHaveBeenCalled();
                                    expect(aUndo).not.toHaveBeenCalled();
                                });       
                                it('does not execute the cancelled callbacks', function(){
                                    expect(bDo).not.toHaveBeenCalled();
                                    expect(bUndo).not.toHaveBeenCalled();
                                });         
                                it('does not execute the new callbacks', function(){
                                    expect(cDo).not.toHaveBeenCalled();
                                    expect(cUndo).not.toHaveBeenCalled();
                                });                                   
                                it('shows a dialog warning to the user', function(){
                                    expect(dialog.show).toHaveBeenCalled();
                                    expect(dialog.show.calls.argsFor(0)[0]).toEqual('undoneChangesWillBeErased');
                                    expect(dialog.show.calls.argsFor(0)[1].ok).toBeDefined();
                                });
                                describe('on clicking ok', function(){
                                    beforeEach(function(){
                                        var ok = dialog.show.calls.argsFor(0)[1].ok;
                                        aDo.calls.reset();
                                        aUndo.calls.reset();
                                        bDo.calls.reset();
                                        bUndo.calls.reset();   
                                        cDo.calls.reset();
                                        cUndo.calls.reset();                                           
                                        dialog.show.calls.reset();
                                        ok();
                                    });
                                    it('allows undo', function(){
                                        expect(service.canUndo()).toBeTruthy();
                                    });
                                    it('does not allow redo', function(){
                                        expect(service.canRedo()).toBeFalsy();
                                    });        
                                    it('does not execute the cancelled callbacks', function(){
                                        expect(bDo).not.toHaveBeenCalled();
                                        expect(bUndo).not.toHaveBeenCalled();
                                    });                                       
                                    it('executes the do callback', function(){
                                        expect(cDo).toHaveBeenCalled();
                                    });
                                    it('does not execute the undo callback', function(){
                                        expect(cUndo).not.toHaveBeenCalled();
                                    });      
                                    it('does not execute the old callbacks', function(){
                                        expect(aDo).not.toHaveBeenCalled();
                                        expect(aUndo).not.toHaveBeenCalled();
                                    }); 
                                    describe('on undoing', function(){
                                        beforeEach(function(){
                                            aDo.calls.reset();
                                            aUndo.calls.reset();
                                            bDo.calls.reset();
                                            bUndo.calls.reset();   
                                            cDo.calls.reset();
                                            cUndo.calls.reset();  
                                            dialog.show = undefined;
                                            service.undo();
                                        });
                                        it('allows redo', function(){
                                            expect(service.canRedo()).toBeTruthy();
                                        });
                                        it('does not allow undo', function(){
                                            expect(service.canUndo()).toBeFalsy();
                                        });        
                                        it('does not execute the old callbacks', function(){
                                            expect(aDo).not.toHaveBeenCalled();
                                            expect(aUndo).not.toHaveBeenCalled();
                                        });       
                                        it('does not execute the cancelled callbacks', function(){
                                            expect(bDo).not.toHaveBeenCalled();
                                            expect(bUndo).not.toHaveBeenCalled();
                                        });         
                                        it('does not execute the new do callback', function(){
                                            expect(cDo).not.toHaveBeenCalled();
                                        });  
                                        it('executes the new undo callback', function(){
                                            expect(cUndo).toHaveBeenCalled();
                                        });        
                                        describe('on doing', function(){
                                            beforeEach(function(){
                                                aDo.calls.reset();
                                                aUndo.calls.reset();
                                                bDo.calls.reset();
                                                bUndo.calls.reset();   
                                                cDo.calls.reset();
                                                cUndo.calls.reset();  
                                                dialog.show = undefined;
                                                service.redo();
                                            });
                                            it('allows undo', function(){
                                                expect(service.canUndo()).toBeTruthy();
                                            });
                                            it('does not allow redo', function(){
                                                expect(service.canRedo()).toBeFalsy();
                                            });        
                                            it('does not execute the old callbacks', function(){
                                                expect(aDo).not.toHaveBeenCalled();
                                                expect(aUndo).not.toHaveBeenCalled();
                                            });       
                                            it('does not execute the cancelled callbacks', function(){
                                                expect(bDo).not.toHaveBeenCalled();
                                                expect(bUndo).not.toHaveBeenCalled();
                                            });         
                                            it('does not execute the new undo callback', function(){
                                                expect(cUndo).not.toHaveBeenCalled();
                                            });  
                                            it('executes the new do callback', function(){
                                                expect(cDo).toHaveBeenCalled();
                                            });                                          
                                        });                                        
                                    });
                                });
                            });
                        });
                        describe('and redoing', function(){
                            beforeEach(function(){
                                aDo.calls.reset();
                                aUndo.calls.reset();
                                bDo.calls.reset();
                                bUndo.calls.reset();   
                                dialog.show.calls.reset();
                                service.redo();
                            });
                            it('allows undo', function(){
                                expect(service.canUndo()).toBeTruthy();
                            });
                            it('does not allow redo', function(){
                                expect(service.canRedo()).toBeFalsy();
                            });        
                            it('executes the do callback', function(){
                                expect(aDo).toHaveBeenCalled();
                            });
                            it('does not execute the undo callback', function(){
                                expect(aUndo).not.toHaveBeenCalled();
                            });      
                            it('does not execute the new callbacks', function(){
                                expect(bDo).not.toHaveBeenCalled();
                                expect(bUndo).not.toHaveBeenCalled();
                            });  
                            describe('and undoing', function(){
                                beforeEach(function(){
                                    aDo.calls.reset();
                                    aUndo.calls.reset();
                                    bDo.calls.reset();
                                    bUndo.calls.reset();   
                                    dialog.show.calls.reset();
                                    service.undo();
                                });
                                it('allows redo', function(){
                                    expect(service.canRedo()).toBeTruthy();
                                });
                                it('does not allow undo', function(){
                                    expect(service.canUndo()).toBeFalsy();
                                });        
                                it('executes the undo callback', function(){
                                    expect(aUndo).toHaveBeenCalled();
                                });
                                it('does not execute the do callback', function(){
                                    expect(aDo).not.toHaveBeenCalled();
                                });      
                                it('does not execute the new callbacks', function(){
                                    expect(bDo).not.toHaveBeenCalled();
                                    expect(bUndo).not.toHaveBeenCalled();
                                });                             
                            });
                        });
                    });
                });
            });
            describe('on redoing that action', function(){
                beforeEach(function(){
                    aDo.calls.reset();
                    aUndo.calls.reset();
                    service.redo();
                });
                it('allows undo', function(){
                    expect(service.canUndo()).toBeTruthy();
                });
                it('does not allow redo', function(){
                    expect(service.canRedo()).toBeFalsy();
                });        
                it('executes the do callback', function(){
                    expect(aDo).toHaveBeenCalled();
                });
                it('does not execute the undo callback', function(){
                    expect(aUndo).not.toHaveBeenCalled();
                });  
                describe('on undoing that action', function(){
                    beforeEach(function(){
                        aDo.calls.reset();
                        aUndo.calls.reset();
                        service.undo();
                    });
                    it('allows redo', function(){
                        expect(service.canRedo()).toBeTruthy();
                    });
                    it('does not allow undo', function(){
                        expect(service.canUndo()).toBeFalsy();
                    });        
                    it('executes the undo callback', function(){
                        expect(aUndo).toHaveBeenCalled();
                    });
                    it('does not execute the do callback', function(){
                        expect(aDo).not.toHaveBeenCalled();
                    });  
                });
                describe('clearing', function(){
                    beforeEach(function(){
                        aDo.calls.reset();
                        aUndo.calls.reset();                
                        service.clear();
                    });
                    it('does not allow undo', function(){
                        expect(service.canUndo()).toBeFalsy();
                    });
                    it('does not allow redo', function(){
                        expect(service.canRedo()).toBeFalsy();
                    });
                    it('does not call the older callbacks', function(){
                        expect(aDo).not.toHaveBeenCalled();
                        expect(aUndo).not.toHaveBeenCalled();
                    });
                    describe('on performing an action', function(){
                        var clearDo, clearUndo;
                        beforeEach(function(){
                            clearDo = jasmine.createSpy();
                            clearUndo = jasmine.createSpy();
                            service.act(clearDo, clearUndo);
                        });
                        it('allows undo', function(){
                            expect(service.canUndo()).toBeTruthy();
                        });
                        it('does not allow redo', function(){
                            expect(service.canRedo()).toBeFalsy();
                        });        
                        it('executes the do callback', function(){
                            expect(clearDo).toHaveBeenCalled();
                        });
                        it('does not execute the undo callback', function(){
                            expect(clearUndo).not.toHaveBeenCalled();
                        });   
                        it('does not call the older callbacks', function(){
                            expect(aDo).not.toHaveBeenCalled();
                            expect(aUndo).not.toHaveBeenCalled();
                        });
                        describe('on undoing that action', function(){
                            beforeEach(function(){
                                clearDo.calls.reset();
                                clearUndo.calls.reset();
                                service.undo();
                            });
                            it('allows redo', function(){
                                expect(service.canRedo()).toBeTruthy();
                            });
                            it('does not allow undo', function(){
                                expect(service.canUndo()).toBeFalsy();
                            });        
                            it('executes the undo callback', function(){
                                expect(clearUndo).toHaveBeenCalled();
                            });
                            it('does not execute the do callback', function(){
                                expect(clearDo).not.toHaveBeenCalled();
                            });         
                            it('does not call the older callbacks', function(){
                                expect(aDo).not.toHaveBeenCalled();
                                expect(aUndo).not.toHaveBeenCalled();
                            });
                            describe('on redoing that action', function(){
                                beforeEach(function(){
                                    clearDo.calls.reset();
                                    clearUndo.calls.reset();
                                    service.redo();
                                });
                                it('allows undo', function(){
                                    expect(service.canUndo()).toBeTruthy();
                                });
                                it('does not allow redo', function(){
                                    expect(service.canRedo()).toBeFalsy();
                                });        
                                it('executes the do callback', function(){
                                    expect(clearDo).toHaveBeenCalled();
                                });
                                it('does not execute the undo callback', function(){
                                    expect(clearUndo).not.toHaveBeenCalled();
                                });  
                                it('does not call the older callbacks', function(){
                                    expect(aDo).not.toHaveBeenCalled();
                                    expect(aUndo).not.toHaveBeenCalled();
                                });
                            });                  
                        });
                    });
                });
            });            
        });
        describe('on performing another action', function(){
            var bDo, bUndo;
            beforeEach(function(){
                aDo.calls.reset();
                aUndo.calls.reset();
                bDo = jasmine.createSpy();
                bUndo = jasmine.createSpy();
                service.act(bDo, bUndo);
            });            
            it('allows undo', function(){
                expect(service.canUndo()).toBeTruthy();
            });
            it('does not allow redo', function(){
                expect(service.canRedo()).toBeFalsy();
            });        
            it('executes the do callback', function(){
                expect(bDo).toHaveBeenCalled();
            });
            it('does not execute the undo callback', function(){
                expect(bUndo).not.toHaveBeenCalled();
            });         
            it('does not execute the other callbacks', function(){
                expect(aDo).not.toHaveBeenCalled();
                expect(aUndo).not.toHaveBeenCalled();
            });
            describe('on undoing', function(){
                beforeEach(function(){
                    aDo.calls.reset();
                    aUndo.calls.reset();
                    bDo.calls.reset();
                    bUndo.calls.reset();                    
                    service.undo();
                });            
                it('allows undo', function(){
                    expect(service.canUndo()).toBeTruthy();
                });
                it('allows redo', function(){
                    expect(service.canRedo()).toBeTruthy();
                });        
                it('executes the undo callback', function(){
                    expect(bUndo).toHaveBeenCalled();
                });
                it('does not execute the other callbacks', function(){
                    expect(aDo).not.toHaveBeenCalled();
                    expect(aUndo).not.toHaveBeenCalled();
                    expect(bDo).not.toHaveBeenCalled();
                }); 
                describe('on undoing', function(){
                    beforeEach(function(){
                        aDo.calls.reset();
                        aUndo.calls.reset();
                        bDo.calls.reset();
                        bUndo.calls.reset();                    
                        service.undo();
                    });            
                    it('does not allow undo', function(){
                        expect(service.canUndo()).toBeFalsy();
                    });
                    it('allows redo', function(){
                        expect(service.canRedo()).toBeTruthy();
                    });        
                    it('executes the undo callback', function(){
                        expect(aUndo).toHaveBeenCalled();
                    });
                    it('does not execute the other callbacks', function(){
                        expect(aDo).not.toHaveBeenCalled();
                        expect(bDo).not.toHaveBeenCalled();
                        expect(bUndo).not.toHaveBeenCalled();  
                    });  
                    describe('on redoing', function(){
                        beforeEach(function(){
                            aDo.calls.reset();
                            aUndo.calls.reset();
                            bDo.calls.reset();
                            bUndo.calls.reset();                    
                            service.redo();
                        });            
                        it('allows undo', function(){
                            expect(service.canUndo()).toBeTruthy();
                        });
                        it('allows redo', function(){
                            expect(service.canRedo()).toBeTruthy();
                        });        
                        it('executes the do callback', function(){
                            expect(aDo).toHaveBeenCalled();
                        });
                        it('does not execute the other callbacks', function(){
                            expect(aUndo).not.toHaveBeenCalled();
                            expect(bUndo).not.toHaveBeenCalled();
                            expect(bDo).not.toHaveBeenCalled();
                        });       
                        describe('on redoing', function(){
                            beforeEach(function(){
                                aDo.calls.reset();
                                aUndo.calls.reset();
                                bDo.calls.reset();
                                bUndo.calls.reset();                    
                                service.redo();
                            });            
                            it('allows undo', function(){
                                expect(service.canUndo()).toBeTruthy();
                            });
                            it('does not allow redo', function(){
                                expect(service.canRedo()).toBeFalsy();
                            });        
                            it('executes the do callback', function(){
                                expect(bDo).toHaveBeenCalled();
                            });
                            it('does not execute the other callbacks', function(){
                                expect(aUndo).not.toHaveBeenCalled();
                                expect(aDo).not.toHaveBeenCalled();
                                expect(bUndo).not.toHaveBeenCalled();
                            });                         
                        });
                    });
                });
            });
        });
        describe('clearing', function(){
            beforeEach(function(){
                aDo.calls.reset();
                aUndo.calls.reset();                
                service.clear();
            });
            it('does not allow undo', function(){
                expect(service.canUndo()).toBeFalsy();
            });
            it('does not allow redo', function(){
                expect(service.canRedo()).toBeFalsy();
            });
            it('does not call the older callbacks', function(){
                expect(aDo).not.toHaveBeenCalled();
                expect(aUndo).not.toHaveBeenCalled();
            });
            describe('on performing an action', function(){
                var clearDo, clearUndo;
                beforeEach(function(){
                    clearDo = jasmine.createSpy();
                    clearUndo = jasmine.createSpy();
                    service.act(clearDo, clearUndo);
                });
                it('allows undo', function(){
                    expect(service.canUndo()).toBeTruthy();
                });
                it('does not allow redo', function(){
                    expect(service.canRedo()).toBeFalsy();
                });        
                it('executes the do callback', function(){
                    expect(clearDo).toHaveBeenCalled();
                });
                it('does not execute the undo callback', function(){
                    expect(clearUndo).not.toHaveBeenCalled();
                });   
                it('does not call the older callbacks', function(){
                    expect(aDo).not.toHaveBeenCalled();
                    expect(aUndo).not.toHaveBeenCalled();
                });
                describe('on undoing that action', function(){
                    beforeEach(function(){
                        clearDo.calls.reset();
                        clearUndo.calls.reset();
                        service.undo();
                    });
                    it('allows redo', function(){
                        expect(service.canRedo()).toBeTruthy();
                    });
                    it('does not allow undo', function(){
                        expect(service.canUndo()).toBeFalsy();
                    });        
                    it('executes the undo callback', function(){
                        expect(clearUndo).toHaveBeenCalled();
                    });
                    it('does not execute the do callback', function(){
                        expect(clearDo).not.toHaveBeenCalled();
                    });         
                    it('does not call the older callbacks', function(){
                        expect(aDo).not.toHaveBeenCalled();
                        expect(aUndo).not.toHaveBeenCalled();
                    });
                    describe('on redoing that action', function(){
                        beforeEach(function(){
                            clearDo.calls.reset();
                            clearUndo.calls.reset();
                            service.redo();
                        });
                        it('allows undo', function(){
                            expect(service.canUndo()).toBeTruthy();
                        });
                        it('does not allow redo', function(){
                            expect(service.canRedo()).toBeFalsy();
                        });        
                        it('executes the do callback', function(){
                            expect(clearDo).toHaveBeenCalled();
                        });
                        it('does not execute the undo callback', function(){
                            expect(clearUndo).not.toHaveBeenCalled();
                        });  
                        it('does not call the older callbacks', function(){
                            expect(aDo).not.toHaveBeenCalled();
                            expect(aUndo).not.toHaveBeenCalled();
                        });
                    });                  
                });
            });
        });        
    });
});