angular.module('luminous-composer').controller('editor', function($rootScope, $scope, history, dialog, constructor, linker, functions, operations){
    $scope.canUndo = history.canUndo;
    $scope.canRedo = history.canRedo;
    $scope.undo = history.undo;
    $scope.redo = history.redo;
    $scope.dialog = dialog;
    $rootScope.node = constructor('sceneVolumetric');
    $scope.generatedCode = function(){
        return linker($scope.node);
    };
    $scope.browser = {
        replacing: null,
        addingTo: null,
        beginReplacing: function(node){
            $scope.browser.replacing = node;
            $scope.browser.addingTo = null;
        },
        beginAddingTo: function(node){
            $scope.browser.replacing = null;
            $scope.browser.addingTo = node;
        },
        cancel: function(){
            $scope.browser.replacing = null;
            $scope.browser.addingTo = null;
        },
        
        // This should be done with a filter, but AngularJS can only filter arrays, not properties of an object.
        // Also, performing the filter manually in a function returning array doesn't mix well with how AngularJS does dirty checking.
        options: []
    };
    
    angular.forEach(functions, function(funct, name){
        $scope.browser.options.push(name);
    });    
    
    $scope.new = function(){
        dialog.show('areYouSureNew', {
            ok: function(){
                $rootScope.node = constructor('sceneVolumetric');
                history.clear();
            }
        });
    };
});