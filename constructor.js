angular.module('luminous-composer').service('constructor', function(functions){
    var service = function(functionName) {
        var func = functions[functionName];
        
        if(func.arrayOf) {
            return {
                type: functionName,
                items: []
            };
        }
        
        if(func.fields) {
            var output = {
                type: functionName,
                fields: {}
            };
            
            angular.forEach(func.fields, function(type, name){
                output.fields[name] = service(type);
            });
            
            return output;
        }
        
        return {
            type: functionName,
            value: func.defaultValue,
            historyValue: func.defaultValue
        };
    };
    
    return service;
});