angular.module('luminous-composer').service('compiler', function(functions){
    var service = function(node) {
        switch(node.type) {
            default:
                var func = functions[node.type];
            
                if(node.items) {
                    if(node.items.length == 0) return func.whenEmpty;
                    if(node.items.length == 1) return service(node.items[0]);
                    var output = '';
                    for(var i = 1; i < node.items.length; i++)
                        output += node.type + '(';
                    output += service(node.items[0]);
                    for(var i = 1; i < node.items.length; i++)
                        output += ', ' + service(node.items[i]) + ')';
                    return output;
                }
                
                if(func.fieldOrder) {
                    var output = node.type + '(';
                    var first = true;
                    for(var field = 0; field < func.fieldOrder.length; field++) {
                        if(first)
                            first = false;
                        else
                            output += ', ';
                        
                        output += service(node.fields[func.fieldOrder[field]]);
                    }
                    return output + ')';
                }
                
                return '' + node.value;
            
            case 'float':
                var float = parseFloat(node.value);
                if(('' + float).indexOf('.') == -1)
                    return float.toFixed(1);
                else
                    return '' + float;
        }
    };
    
    return service;
});