describe('node', function(){
    var rootScope, scope, history, operations;
    beforeEach(function(){
        module('luminous-composer');
        inject(function($controller){
            $controller('node', {$rootScope: rootScope = {}, $scope: scope = {}, history: history = {}, operations: operations = {}});
        });
    });
    describe('browserTitle', function(){
        beforeEach(function(){
            scope.node = 'Test Node';
            scope.browser = {
                addingTo: null,
                replacing: null
            };
        });
        it('returns null when nothing is being replaced or added to', function(){
            expect(scope.browserTitle()).toBeFalsy();
        });
        it('returns null when another node is being replaced', function(){
            scope.browser.replacing = {};
            expect(scope.browserTitle()).toBeFalsy();
        });      
        it('returns null when another node is being added to', function(){
            scope.browser.addingTo = {};
            expect(scope.browserTitle()).toBeFalsy();
        });         
        it('returns "browserTitleReplacing" when the node is being replaced', function(){
            scope.browser.replacing = 'Test Node';
            expect(scope.browserTitle()).toEqual('browserTitleReplacing');
        });      
        it('returns "browserTitleAddTo" when the node is being added to', function(){
            scope.browser.addingTo = 'Test Node';
            expect(scope.browserTitle()).toEqual('browserTitleAddingTo');
        });           
    });
    describe('replace', function(){
        it('has the browser begin replacing the node', function(){
            scope.node = 'Test Node';
            scope.browser = {
                beginReplacing: jasmine.createSpy()
            };
            
            scope.replace();
            
            expect(scope.browser.beginReplacing).toHaveBeenCalledWith('Test Node');
        });
    });
    describe('add', function(){
        it('has the browser begin adding to the node', function(){
            scope.node = 'Test Node';
            scope.browser = {
                beginAddingTo: jasmine.createSpy()
            };
            
            scope.add();
            
            expect(scope.browser.beginAddingTo).toHaveBeenCalledWith('Test Node');
        });
    }); 
    describe('remove', function(){
        var parent;
        beforeEach(function(){
            scope.node = 'Test Node';
            rootScope.node = 'Test Root Node';
            history.act = jasmine.createSpy();
            parent = {
                items: [
                    'Ignore A',
                    'Test Node',
                    'Ignore B'
                ]
            };
            operations.findParent = jasmine.createSpy();
            operations.findParent.and.returnValue(parent);
            scope.remove();
        });
        it('finds the parent of the node', function(){
            expect(operations.findParent).toHaveBeenCalledWith('Test Root Node', 'Test Node');
        });
        it('registers a new history step', function(){
            expect(history.act).toHaveBeenCalled();
        });
        describe('on performing the history step', function(){
            beforeEach(function(){
                history.act.calls.argsFor(0)[0]();
            });
            it('removes the node from the parent array', function(){
                expect(parent).toEqual({
                    items: [
                        'Ignore A',
                        'Ignore B'
                    ]
                })
            });
        });
        describe('on performing the history step', function(){
            beforeEach(function(){
                parent.items = [
                    'Ignore A',
                    'Ignore B'
                ];
                history.act.calls.argsFor(0)[1]();
            });
            it('removes the node from the parent array', function(){
                expect(parent).toEqual({
                    items: [
                        'Ignore A',
                        'Test Node',
                        'Ignore B'
                    ]
                })
            });
        });        
    });
    describe('persist', function(){
        beforeEach(function(){
            history.act = jasmine.createSpy();
        });
        describe('when the value hasn\'t changed', function(){
            beforeEach(function(){
                scope.node = {
                    value: 4.5,
                    historyValue: 4.5
                };
                scope.persist();
            });
            it('does not register a new history step', function(){
                expect(history.act).not.toHaveBeenCalled();
            });
        });
        describe('when the value has changed', function(){
            beforeEach(function(){
                scope.node = {
                    value: 4.5,
                    historyValue: 2.1
                };
                scope.persist();
            });            
            it('returns the value to what was in the history, so if the history step is rejected, the changes are undone', function(){
                expect(scope.node.value).toEqual(2.1);
            });
            it('registers a new history step', function(){
                expect(history.act).toHaveBeenCalled();
            });
            describe('on performing the history step', function(){
                beforeEach(function(){
                    history.act.calls.argsFor(0)[0]();
                });
                it('sets value to the new value', function(){
                    expect(scope.node.value).toEqual(4.5);
                });
                it('sets historyValue to the new value', function(){
                    expect(scope.node.historyValue).toEqual(4.5);
                });                
                describe('on undoing the history step', function(){
                    beforeEach(function(){
                        history.act.calls.argsFor(0)[1]();
                    });                
                    it('sets value to the old value', function(){
                        expect(scope.node.value).toEqual(2.1);
                    });
                    it('sets historyValue to the old value', function(){
                        expect(scope.node.historyValue).toEqual(2.1);
                    });                
                });                 
            });
        });
    });
});