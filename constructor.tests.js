describe('constructor', function(){
    var run = function(node, _functions){
        var result;
        module('luminous-composer');
        inject(function(constructor, functions){
            // We can't mock an AngularJS constant for some reason, so instead
            // all we can do is replace its contents.
            var toRemove = [];
            angular.forEach(functions, function(unused, key){
                toRemove.push(key);
            });
            angular.forEach(toRemove, function(key){
                delete functions[key];
            });
            angular.copy(_functions, functions);
            
            result = constructor(node);
        });
        return result;
    };
    describe('non-functions and non-arrays', function(){
        it('copies the type name', function(){
            expect(run('valueB', {
                valueA: {},
                valueB: {},
                valueC: {}
            }).type).toEqual('valueB');
        });
        it('copies the default value', function(){
            expect(run('valueB', {
                valueA: {
                    defaultValue: 6.7
                },
                valueB: {
                    defaultValue: 3.6
                },
                valueC: {
                    defaultValue: 7.8
                }
            }).value).toEqual(3.6);
        });  
        it('copies the default value,', function(){
            expect(run('valueB', {
                valueA: {
                    defaultValue: 6.7
                },
                valueB: {
                    defaultValue: 3.6
                },
                valueC: {
                    defaultValue: 7.8
                }
            }).historyValue).toEqual(3.6);
        });          
        it('does not define fields', function(){
            expect(run('valueB', {
                valueA: {},
                valueB: {},
                valueC: {}
            }).fields).not.toBeDefined();
        });
        it('does not define items', function(){
            expect(run('valueB', {
                valueA: {},
                valueB: {},
                valueC: {}
            }).items).not.toBeDefined();
        });        
    });
    describe('functions', function(){
        it('copies the type name', function(){
            expect(run('callB', {
                callA: {fields: {}},
                callB: {fields: {}},
                callC: {fields: {}}
            }).type).toEqual('callB');            
        });
        it('does not define items', function(){
            expect(run('callB', {
                callA: {fields: {}},
                callB: {fields: {}},
                callC: {fields: {}}
            }).items).not.toBeDefined();
        });        
        it('supports value fields', function(){
            expect(run('callB', {
                valueA: {
                    defaultValue: 3.0
                },
                valueB: {
                    defaultValue: -8
                },
                callA: {fields: {
                    fieldAA: "valueA",
                    fieldAB: "valueB",
                }},
                callB: {fields: {
                    fieldBA: "valueB",
                    fieldBB: "valueA",                    
                }},
                callC: {fields: {}}
            }).fields).toEqual({
                fieldBA: {
                    type: "valueB",
                    value: -8,
                    historyValue: -8
                },
                fieldBB: {
                    type: "valueA",
                    value: 3.0,
                    historyValue: 3.0
                }
            });             
        });
        it('supports function fields', function(){
            expect(run('callB', {
                valueA: {
                    defaultValue: 3.0
                },
                valueB: {
                    defaultValue: -8
                },
                callA: {fields: {
                    fieldAA: "valueA",
                    fieldAB: "valueB",
                }},
                callB: {fields: {
                    fieldBA: "callA",
                    fieldBB: "valueA",                    
                }},
                callC: {fields: {}}
            }).fields).toEqual({
                fieldBA: {
                    type: "callA",
                    fields: {
                        fieldAA: {
                            type: "valueA",
                            value: 3.0,
                            historyValue: 3.0
                        },
                        fieldAB: {
                            type: "valueB",
                            value: -8,
                            historyValue: -8
                        }
                    }
                },
                fieldBB: {
                    type: "valueA",
                    value: 3.0,
                    historyValue: 3.0
                }
            });  
        });
        it('supports array fields', function(){
            expect(run('callB', {
                arrayA: {arrayOf: "float"},
                arrayB: {arrayOf: "float"},
                arrayC: {arrayOf: "float"},
                valueA: {
                    defaultValue: 3.0
                },
                valueB: {
                    defaultValue: -8
                },
                callA: {fields: {
                    fieldAA: "valueA",
                    fieldAB: "valueB",
                }},
                callB: {fields: {
                    fieldBA: "arrayB",
                    fieldBB: "valueA",                    
                }},
                callC: {fields: {}}
            }).fields).toEqual({
                fieldBA: {
                    type: "arrayB",
                    items: []
                },
                fieldBB: {
                    type: "valueA",
                    value: 3.0,
                    historyValue: 3.0
                }
            });             
        });
    });
    describe('arrays', function(){
        it('copies the type name', function(){
            expect(run('arrayB', {
                arrayA: {arrayOf: "float"},
                arrayB: {arrayOf: "float"},
                arrayC: {arrayOf: "float"}
            }).type).toEqual('arrayB');            
        });
        it('does not define fields', function(){
            expect(run('arrayB', {
                arrayA: {arrayOf: "float"},
                arrayB: {arrayOf: "float"},
                arrayC: {arrayOf: "float"}
            }).fields).not.toBeDefined();            
        });      
        it('defines an empty array of items', function(){
            expect(run('arrayB', {
                arrayA: {arrayOf: "float"},
                arrayB: {arrayOf: "float"},
                arrayC: {arrayOf: "float"}
            }).items).toEqual([]);
        });           
    })
});