angular.module('luminous-composer').service('operations', function(){
    var walk = function(node, callback, parent){
        callback(node, parent);
        
        angular.forEach(node.fields || node.items, function(field){
            walk(field, callback, node);
        });
    };
    
    var service = {
        replace: function(nodeContainer, toReplace, replaceWith){
            if(nodeContainer.node == toReplace)
                nodeContainer.node = replaceWith;
                
            walk(nodeContainer.node, function(node){
                var list = node.fields || node.items;
                angular.forEach(list, function(value, key){
                    if(value == toReplace)
                        list[key] = replaceWith;
                });
            });
        },
        findParent: function(root, find) {
            var _parent;
            walk(root, function(node, parent){
                if(node == find) _parent = parent;
            });
            return _parent;
        }
    };
    
    return service;
});