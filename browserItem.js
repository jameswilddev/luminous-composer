angular.module('luminous-composer').controller('browserItem', function($rootScope, $scope, functions, history, constructor, operations){
    $scope.available = function(){
        if($scope.browser.replacing)
            return functions[$scope.node.type].returns == functions[$scope.option].returns;
            
        if($scope.browser.addingTo)
            return functions[$scope.node.type].arrayOf == functions[$scope.option].returns;
    };
    
    $scope.use = function(){
        if($scope.browser.replacing) {
            var existingInstance = $scope.node;
            var newInstance = constructor($scope.option);
            newInstance.arrayItem = existingInstance.arrayItem;
            $scope.browser.replacing = null;
            
            history.act(function(){
                operations.replace($rootScope, existingInstance, newInstance);
            }, function(){
                operations.replace($rootScope, newInstance, existingInstance);
            });
        }
        
        if($scope.browser.addingTo) {
            var parentArray = $scope.node;
            var newInstance = constructor($scope.option);
            newInstance.arrayItem = true;
            $scope.browser.addingTo = null;
            
            history.act(function(){
                parentArray.items.push(newInstance);
            }, function(){
                parentArray.items.length--;
            });
        }
    };
});